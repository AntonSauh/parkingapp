import * as moment from 'moment';
import { ServerService } from "./server.service";

export class InvoiceGeneratorService {
    fees: any;

    // Set Parking Costs To fees Array
    getParkingFees(data) {
        this.fees = data;
    }
    // // Returns array of parking record
    // With parking fee record added  to array
    TimeCalculator(dataOnResponse) {
        let totalFee: number = 0;
        dataOnResponse.forEach(element => {
            //Getting prices for each element depending on elements userType and parkLotName
            var prices = {
                'dayPrice': 0,
                'nightPrice': 0
            }
            function check(obj) {
                if(obj.parkingName == element.parkLotName && obj.userType == element.userType) {
                    prices.dayPrice = obj.priceDay;
                    prices.nightPrice = obj.priceNight;
                }
            }
            this.fees.filter(check);


            //Binding local variables
            // Starting Date
            var start = moment(element.startTime);
            // End Date
            var end = moment(element.endTime);
            // Dummy dates for calculations
            var dummy1start = moment(element.startTime).set('hour', 7);
            var dummy2start = moment(element.endTime).set('hour', 7);
            var dummy1end = moment(element.startTime).set('hour', 19);
            var dummy2end = moment(element.endTime).set('hour', 19);
            // Formatting dates for further output in invoice
            element.startTime = moment(element.startTime).format("dddd, MMMM Do YYYY, hh:mm:ss");
            element.endTime = moment(element.endTime).format("dddd, MMMM Do YYYY, hh:mm:ss");
            // start and stop difference less than 1 day
            // TODO: LOGIC FOR HANGDLING < 1 DAY REQUESTS BUT START AND FINISH DIFFERENT DAYS
            if(end.diff(start, 'days') == 0) {
                if (end.get('date') == start.get('date')) {
                    if(end.get('hour') < 7) {
                        var timeDifference1 = end.diff(start, 'minute') / 30;
                        element.fee = (Math.ceil(timeDifference1) * prices.dayPrice);
                        totalFee = totalFee + element.fee;
                        console.log(element.fee);

                    }
                    else if(end.get('hour') >= 7 && end.get('hour') <= 19) {
                        if(start.get('hour') < 7) {
                            var timeDifference1 = Math.ceil(dummy1start.diff(start, 'minute') / 30) * prices.nightPrice;
                            var timeDifference2 = Math.ceil(end.diff(dummy1start, 'minute') / 30) * prices.dayPrice;
                            element.fee = timeDifference1 + timeDifference2;
                            totalFee = totalFee + element.fee;
                            console.log(element.fee);
                            
                        }else {
                            var timeDifference1 = Math.ceil(dummy1end.diff(start, 'minute') / 30) * prices.dayPrice;
                            element.fee = timeDifference1;
                            totalFee = totalFee + element.fee;
                            console.log(element.fee);
                        
                        }

                    }else {
                        if(start.get('hour') < 7) {
                            var timeDifference1 = Math.ceil(dummy1start.diff(start, 'minute') / 30) * prices.nightPrice;
                            var timeDifference2 = Math.ceil(dummy1end.diff(dummy1start, 'minute') / 30) * prices.dayPrice;
                            var timeDifference3 = Math.ceil(end.diff(dummy1end, 'minute') / 30) * prices.nightPrice;
                            element.fee = (timeDifference1 + timeDifference2 + timeDifference3);
                            totalFee = totalFee + element.fee;
                            console.log(element.fee);
                        } else if(start.get('hour') >= 7 && start.get('hour') <= 19) {
                            var timeDifference1 = Math.ceil(dummy1end.diff(start, 'minute') / 30) * prices.dayPrice;
                            var timeDifference2 = Math.ceil(end.diff(dummy1end, 'minute') / 30) * prices.nightPrice;
                            element.fee = (timeDifference1 + timeDifference2);
                            totalFee = totalFee + element.fee;
                            console.log(element.fee);
                        }else {
                            var timeDifference1 = Math.ceil(end.diff(start, 'hour') / 30) * prices.nightPrice;
                            element.fee = timeDifference1;
                            totalFee = totalFee + element.fee;
                            console.log(element.fee);
                        }
                    }
                }
            }

            // TODO: LOGIC FOR HANDLING REQUESTS LONGER THAN 1 DAY
            // start and stop difference more than 1 day
            else if(end.diff(start, 'days') == 1) {

                if(start.get('hour') < 7) {
                    if(end.get('hour') <= 19 && end.get('hour') >= 7) {
                        var timeDifference1 = dummy1start.diff(start, 'minute') / 30;
                        var timeDifference2 = dummy1end.diff(dummy1start, 'minute') / 30;
                        var timeDifference3 = dummy2start.diff(dummy1end, 'minute') / 30;
                        var timeDifference4 = end.diff(dummy2start, 'minute') / 30;
                    }else if(end.get('hour') > 19 && end.get('hour') <= 24) {
                        var timeDifference1 = dummy1start.diff(start, 'minute') / 30;
                        var timeDifference2 = dummy2start.diff(dummy1start, 'minute') / 30;
                        var timeDifference3 = dummy1end.diff(dummy2start, 'minute') / 30;
                        var timeDifference4 = dummy2end.diff(dummy1end, 'minute') / 30;
                        var timeDifference5 = end.diff(dummy2end,'minute') / 30;
                    }else if(end.get('hour') < 7) {
                        var changedDate = end.get('date') - 1;
                        dummy2start = end.set({'date': changedDate,'hour': 7, 'minutes': 0, 'second': 0, 'millisecond':0 });
                        dummy2end = end.set({'date': changedDate,'hour': 19, 'minutes': 0, 'second': 0, 'millisecond':0 });
                        var timeDifference1 = dummy1start.diff(start, 'minute') / 30;
                        var timeDifference2 = dummy2start.diff(dummy1start, 'minute') / 30;
                        var timeDifference3 = dummy1end.diff(dummy2start, 'minute') / 30;
                        var timeDifference4 = dummy2end.diff(dummy1end, 'minute') / 30;
                        var timeDifference5 = end.diff(dummy2end,'minute') / 30;                        

                    }
                }else if(start.get('hour') <= 19) {
                    if(end.get('hour')){

                    }

                }

            }
            //start and stop difference more than 1 day
            else {
            }
                    
            const userType = element.userType;
            const parkLot = element.parkLotName;

                });
        dataOnResponse.totalFee = totalFee;
        return dataOnResponse;
    }
    // Gets data from DB
    //  Passes it to other methods for calculation and formatting
    //  Returns array of objects, which consits of all data, needed to fill the invoice.
    generateInvoiceData(data) {
        const response = this.TimeCalculator(data);
        return response;
    }

}
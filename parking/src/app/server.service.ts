import {Injectable} from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/Rx';
import { InvoiceGeneratorService } from "./invoice-generator.service";

@Injectable()
export class ServerService {
    constructor (private http: Http, private invoiceGenerator: InvoiceGeneratorService) {}

    getUsersIds() {
        return this.http.get('api/getusersids').map(
            (res) => {
                const data = res.json();
                return data;
            }
        )
    }
    getUserById(userId, userType) {
        return this.http.get('api/getuserbyid/' + userId + "/" + userType).map(
            (res) => {
                
                 const DataOnResponse = res.json();
                 const SingleArray = DataOnResponse[0].concat(DataOnResponse[1]);
                 const DataToReturn = {};
                 for(var key in SingleArray[0]) DataToReturn[key] = SingleArray[0][key];
                 for(var key in SingleArray[1]) DataToReturn[key] = SingleArray[1][key];
                
                return DataToReturn;
            }
        )
    }
    getParkingPricesForUserType(userType) {
        return this.http.get('api/getparkingprice/' + userType).map(
        (res) => {
            const data = res.json();
            return data;
        }
        )
    }
    getParkingPrices() {
        return this.http.get('api/getparkingprice').map(
            (res) => {
                const dataOnResponse = res.json();
                this.invoiceGenerator.getParkingFees(dataOnResponse);
                return dataOnResponse;
            }
        )
    }

    getUserParkingsById(userId) {
        return this.http.get('api/getinvoice/' + userId).map(
            (res) => {
                let dataOnResponse = res.json();
                dataOnResponse = this.invoiceGenerator.generateInvoiceData(dataOnResponse);
                return dataOnResponse;
            }
        )
    }
    getUserFeesByUT(userType) {
        console.log(userType);
        return this.http.get('api/getuserfeebyUt/' + userType).map(
            (res) => {
                const dataOnResponse = res.json();
                return dataOnResponse;
            }
        )

    }
}
import { Component, Injectable, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { loggingService } from "../logging.service";
import {ServerService} from "../server.service";

@Component({
  selector: 'parking-admin-root',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
@Injectable()
export class ParkingAdminRootComponent implements OnInit {
  usersids = [];
  
  constructor(private router :Router, private loggingService: loggingService, private serverService: ServerService) {}
  ngOnInit() {
    this.serverService.getUsersIds().subscribe(
      (usersids: any[]) => this.usersids = usersids,
      (error) => console.log (error)
    );
  }
  userSelected(uId,uT) {
    this.loggingService.logStatus("user with id of: " + uId + " selection");
    this.router.navigate(['user', uId], {queryParams: {userType: uT}});
    
  }
  
}
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { loggingService } from "../../logging.service";
import { ServerService } from "../../server.service";

@Component({
  selector: 'parking-admin-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class ParkingAdminUserComponent implements OnInit {
    userData: any = {
        'userId': ''
    }
    Parkings: [{
        parkingName: string,
        priceDay: number,
        priceNight: number
    }];
    constructor(private route: ActivatedRoute, private router :Router, private loggingService: loggingService, private serverService: ServerService)  {

    }
    ngOnInit() {
    
    this.serverService.getUserById(this.route.snapshot.params['id'], this.route.snapshot.queryParams.userType).subscribe(
        
      (user: number) => this.userData = user,
      (error) => console.log (error)
    );

    this.serverService.getParkingPricesForUserType(this.route.snapshot.queryParams.userType).subscribe(
        (parking: any) => this.Parkings = parking,
        (error) => console.log(error)
    );
    }
    generateInvoice() {
        this.loggingService.logStatus("Invoice Generation Start");
        const invoiceId = Math.floor(Math.random() * (99999 - 10000 + 1)) + 10000;
        this.router.navigate(['/user',this.userData.userId,'userType',this.userData.userType,'invoice', invoiceId]);
    }
}
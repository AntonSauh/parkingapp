import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { loggingService } from "../../../logging.service";
import { ServerService } from "../../../server.service";

@Component({
  selector: 'parking-admin-user-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class UserInvoiceComponent implements OnInit {
    user: { id: string,
        invoiceId: number,
        userType: string,
    };
    
    userInvoices = [{}];
    constructor(private route: ActivatedRoute, private router :Router, private loggingService: loggingService, private serverService: ServerService)  {

    }

    userFees = {
        'userMaxInvoice': '',
        'userSubFee': ''
    }

    ngOnInit() {
        this.user = {
            id: this.route.snapshot.params['id'],
            invoiceId: this.route.snapshot.params['invoiceid'],
            userType: this.route.snapshot.params['ut'],
        }
        this.serverService.getUserFeesByUT(this.user.userType).subscribe(
            (feeData) => this.userFees = feeData[0],
            (error) => console.log(error),

            
        );
        this.serverService.getParkingPrices().subscribe();
        this.serverService.getUserParkingsById(this.user.id).subscribe(
            (userinvs: any[]) => this.userInvoices = userinvs,
            (error) => console.log (error),
            
        );
        this.loggingService.logStatus("user invoice with id:" + this.user.invoiceId + " loaded");
    }
    backToUser(){
        this.loggingService.logStatus("Going Back to User Page with id:" + this.user.id);
        this.router.navigate(['/user',this.user.id]);
    }
}
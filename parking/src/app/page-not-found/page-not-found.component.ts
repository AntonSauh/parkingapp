import { Component, Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { loggingService } from "../logging.service";

@Component({
  selector: 'page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent {
    constructor(private router :Router, private loggingService: loggingService) {}

    homeButton() {
        this.router.navigate(['']);
        this.loggingService.logStatus("Navigating Home");
    }
}
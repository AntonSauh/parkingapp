import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import {ParkingAdminRootComponent} from './admin/admin.component';
import {ParkingAdminUserComponent} from './admin/user/user.component';
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { loggingService } from "./logging.service";
import { ServerService } from "./server.service";
import { UserInvoiceComponent } from "./admin/user/invoice/invoice.component";
import { InvoiceGeneratorService } from "./invoice-generator.service";

const appRoutes: Routes = [
  {
    path:'',
    component: ParkingAdminRootComponent
  },
  {
    path:'user/:id',
    component: ParkingAdminUserComponent
  },
  {
    path: 'user/:id/userType/:ut/invoice/:invoiceid',
    component: UserInvoiceComponent
  },
  {
    path:'not-found',
    component:PageNotFoundComponent
  },
  {
    path:'**',
    redirectTo: '/not-found'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ParkingAdminRootComponent,
    ParkingAdminUserComponent,
    PageNotFoundComponent,
    UserInvoiceComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [loggingService, ServerService, InvoiceGeneratorService],
  bootstrap: [AppComponent]
})
export class AppModule { }

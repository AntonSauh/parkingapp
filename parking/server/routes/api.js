const express = require('express');
const router = express.Router();
const mysql = require('mysql');

// Connect parameters
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password:'root',
    port: '8889',
    database: 'parking',
    multipleStatements: true
});
//Creates connection
connection.connect((err) => {
  if(err){
    console.log(err);
    return;
  }
  console.log('Connection established');
});
// Gets Ids and userType of All existing users
router.get('/getusersids', (req, res) => {
    console.log("GET REQUST TO /getuserids CONFIRMED")
    connection.query("SELECT userId, userType FROM users", function (err, result, fields) {
        if (err) throw err;
        res.send(result);
        console.log("/getusersids query GOOD");
  });  
});
// Gets User Info By userId and UserType
router.get('/getuserbyid/:uid/:ut', (req, res) => {
    const userId = req.params.uid;
    const userType = req.params.ut;
    console.log("GET /getuserinfo:" + userId);
// Better way to do this is to use async library. Executing multiple quaries on one httprequest.
    connection.query("SELECT * FROM users WHERE userId = ?;" + 
    "SELECT userSubFee, userMaxInvoice FROM userTypes WHERE userType = ?"
    , [userId, userType], (err, result, fields)=> {
        if (err) throw err;
        res.send(result);
        console.log("/getuserbyid GOOD");
    });
});
    router.get('/getuserfeebyUt/:ut', (req,res) => {
        const userType = req.params.ut;
        console.log("GET /getuserfeebyUt:" + userType);
        connection.query("SELECT userSubFee, userMaxInvoice FROM userTypes WHERE userType = ?", userType, (err, result, fields) => {
            if (err) throw err;
            res.send(result);
            console.log("getuserfeebyUt GOOD");
        })
    });
// Get Subscription Fee and Max Invoice For Selected User Type
    router.get('/getparkingprice/:ut', (req, res) => {
        const userType = req.params.ut;
        console.log("GET /getparkingprice:" + userType);
        connection.query("SELECT parkingName, priceDay, priceNight FROM parkingLots WHERE userType = ?", userType,
        (err, result, fields) => {
            if(err) throw err;
            res.send(result);
            console.log("/getparkingprice for userType GOOD");
        })        
    });
//Get parking prices for every user type at every parking lot
    router.get('/getparkingprice', (req, res) => {
        console.log("GET /getparkingprice");
        connection.query("SELECT parkingName, userType, priceDay, priceNight FROM parkingLots",
        (err, result, fields) => {
            if(err) throw err;
            res.send(result);
            console.log("/getparkingprice GOOD");
        })        
    });
// Get User Finished Parking Events (where parkingStatus = 0)
    router.get('/getinvoice/:uid', (req, res) => {
        const userId = req.params.uid;
        console.log("GET /getinvoice for user id: " + userId);
        connection.query("SELECT userType, parkLotName, startTime, endTime FROM parkingEvenets WHERE userId =" +
        userId + " AND parkingStatus = 0", (err, result, fields) => {
            if(err) throw err;
            console.log("/getinvoice DONE");
            res.send(result);
        })
    })

module.exports = router;
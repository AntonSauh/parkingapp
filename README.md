# README #

Take this steps to setup project on your machine.

### How do I get set up? ###

Clone repository.

Go into the repo folder and deploy arv_parking.sql on your SQL database.

In the parking folder run npm install.

In the api.js change connection parameters.

In the parking folder run ng build.

To run the app run node server inside the parking folder.

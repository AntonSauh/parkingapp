-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 04, 2017 at 04:43 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parking`
--
CREATE DATABASE IF NOT EXISTS `parking` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `parking`;

-- --------------------------------------------------------

--
-- Table structure for table `parkingEvenets`
--

DROP TABLE IF EXISTS `parkingEvenets`;
CREATE TABLE `parkingEvenets` (
  `userId` int(11) NOT NULL,
  `userType` varchar(60) NOT NULL,
  `parkLotName` varchar(60) NOT NULL,
  `startTime` timestamp NULL DEFAULT NULL,
  `endTime` timestamp NULL DEFAULT NULL,
  `parkingStatus` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parkingEvenets`
--

INSERT INTO `parkingEvenets` (`userId`, `userType`, `parkLotName`, `startTime`, `endTime`, `parkingStatus`) VALUES
(1, 'Premium', 'parkingA', '2017-08-31 21:00:00', '2017-09-01 09:00:00', 0),
(1, 'Premium', 'parkingA', '2017-08-31 21:00:00', '2017-09-01 09:30:00', 0),
(1, 'Premium', 'parkingA', '2017-08-31 21:00:00', '2017-09-01 09:29:00', 0),
(1, 'Premium', 'parkingA', '2017-08-31 21:00:00', '2017-09-01 09:32:00', 0),
(1, 'Premium', 'parkingA', '2017-09-01 08:00:00', '2017-09-01 20:00:00', 0),
(1, 'Premium', 'parkingA', '2017-09-04 03:09:00', '2017-09-04 04:37:00', 0),
(1, 'Premium', 'parkingA', '2017-09-04 17:00:00', '2017-09-04 18:00:00', 0),
(2, 'Regular', 'parkingA', '2017-08-31 21:00:00', '2017-09-01 17:00:00', 0),
(2, 'Regular', 'parkingA', '2017-09-03 02:36:00', '2017-09-03 09:50:00', 0),
(2, 'Regular', 'parkingA', '2017-09-04 11:00:00', '2017-09-04 13:44:00', 0),
(2, 'Regular', 'parkingA', '2017-09-05 15:37:22', '2017-09-05 17:00:00', 0),
(1, 'Premium', 'parkingA', '2017-09-03 21:00:00', '2017-09-04 20:59:00', 0),
(1, 'Premium', 'parkingA', '2017-09-03 21:00:00', '2017-09-04 20:59:00', 0),
(1, 'Premium', 'parkingA', '2017-09-05 21:59:00', '2017-09-06 20:00:00', 0),
(1, 'Premium', 'parkingA', '2017-09-06 20:59:00', '2017-09-06 20:59:00', 0),
(1, 'Premium', 'parkingA', '2017-09-03 21:00:00', '2017-09-04 20:00:00', 0),
(1, 'Premium', 'parkingA', '2017-08-31 21:11:00', '2017-09-01 20:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `parkingLots`
--

DROP TABLE IF EXISTS `parkingLots`;
CREATE TABLE `parkingLots` (
  `parkingName` varchar(60) NOT NULL,
  `userType` varchar(60) NOT NULL,
  `priceDay` float NOT NULL,
  `priceNight` float NOT NULL,
  `parkingLotStatus` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parkingLots`
--

INSERT INTO `parkingLots` (`parkingName`, `userType`, `priceDay`, `priceNight`, `parkingLotStatus`) VALUES
('parkingA', 'Regular', 1.5, 1, 1),
('parkingA', 'Premium', 1, 0.75, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `userType` varchar(60) NOT NULL,
  `userName` varchar(60) NOT NULL,
  `userStatus` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userType`, `userName`, `userStatus`) VALUES
(1, 'Premium', 'Sergei', 1),
(2, 'Regular', 'Anton', 1);

-- --------------------------------------------------------

--
-- Table structure for table `userTypes`
--

DROP TABLE IF EXISTS `userTypes`;
CREATE TABLE `userTypes` (
  `userType` varchar(60) NOT NULL,
  `userState` tinyint(1) NOT NULL,
  `userSubFee` float DEFAULT NULL,
  `userMaxInvoice` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userTypes`
--

INSERT INTO `userTypes` (`userType`, `userState`, `userSubFee`, `userMaxInvoice`) VALUES
('Premium', 1, 20, 300),
('Regular', 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `parkingEvenets`
--
ALTER TABLE `parkingEvenets`
  ADD KEY `userId` (`userId`),
  ADD KEY `parkingevenets_ibfk_2` (`userType`);

--
-- Indexes for table `parkingLots`
--
ALTER TABLE `parkingLots`
  ADD KEY `Constr` (`userType`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD KEY `userType` (`userType`);

--
-- Indexes for table `userTypes`
--
ALTER TABLE `userTypes`
  ADD PRIMARY KEY (`userType`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `parkingEvenets`
--
ALTER TABLE `parkingEvenets`
  ADD CONSTRAINT `parkingevenets_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`),
  ADD CONSTRAINT `parkingevenets_ibfk_2` FOREIGN KEY (`userType`) REFERENCES `userTypes` (`userType`);

--
-- Constraints for table `parkingLots`
--
ALTER TABLE `parkingLots`
  ADD CONSTRAINT `Constr` FOREIGN KEY (`userType`) REFERENCES `userTypes` (`userType`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`userType`) REFERENCES `userTypes` (`userType`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
